(function(root) {
	var SnakeGame = root.SnakeGame = (root.SnakeGame || {});
	
	var View = SnakeGame.View = function($el) {
		this.$el = $el;
		this.board = {};
	}
	
	View.prototype.start = function() {
		this.board = new SnakeGame.Board();
		$(window).keydown(this.handleKeyEvent.bind(this));
		window.setInterval( this.step.bind(this) ,500);
	}
	
	View.prototype.step = function() {
		console.log("stepping!")
		this.board.snake.move();
		console.log(this.board.snake.segments[0]);
		this.$el.html(this.render());
	}
	
	View.prototype.handleKeyEvent = function(event) {
		switch(event.keyCode) {
			case 37: //left
			console.log("LEFT!")
				this.board.snake.turn("W");
				break;			
			case 38: //up
						console.log("UP!")
				this.board.snake.turn("N");
				break;
			
			case 39: //right
						console.log("RIGHT!")
				this.board.snake.turn("E");
				break;
			
			case 40: //down
						console.log("DOWN!")
				this.board.snake.turn("S");				
				break;
		}
	} 
	
	View.prototype.render = function() {
		$grid = $('<div class="game"></div>')
		for( i = 0; i < SnakeGame.Board.YDIM; i++ ) {
			$row = $('<div class="row"></div>')
			for( j = 0; j< SnakeGame.Board.XDIM; j++ ) {
				$cell = $('<div class="cell"></div>')
				if(this.board.snake.isSnake(i, j)) {
					$cell.addClass('snake');
				}
				$row.append($cell)	
			}
			$grid.append($row);
		};
		
		
		return $grid;
	}
})(this);