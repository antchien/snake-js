(function(root) {
	var SnakeGame = root.SnakeGame = (root.SnakeGame || {});
	var Snake = SnakeGame.Snake = function(x, y) {
		this.dir = "N";
		this.segments = [new Coord(x,y)];
		
	}
	
	Snake.prototype.move = function() {
		oldHead = this.segments[0];
		newHead = new Coord(oldHead.y, oldHead.x)
		switch(this.dir) {
			case "N":
				newHead.y -= 1;
				break;
			case "S": 
				newHead.y += 1;
				break;
			case "E":
				newHead.x += 1;
				break;
			case "W":
				newHead.x -= 1;
				break;
		}
		this.segments.unshift(newHead);
		this.segments.pop();
	}
	
	Snake.prototype.turn = function(dir) {
		this.dir = dir;
	}
	
	Snake.prototype.isSnake = function(y, x) {
		var conflict = false;
		_.each(this.segments, function(segment) {
			if (segment.y === y && segment.x === x) {
				conflict = true;
			}
		})
		return conflict;
	}
	
	var Board = SnakeGame.Board = function() {
		var centerX = Math.floor( Board.XDIM / 2 );
		var centerY = Math.floor( Board.YDIM / 2 );
		this.snake = new Snake(centerX, centerY)
		this.grid = new Array(Board.YDIM);
		for( i = 0; i < Board.YDIM; i++ ) {
			this.grid[i] = [];
			for( j = 0; j< Board.XDIM; j++ ) {
				this.grid[i].push(".");
			};
		};
		
	};
	Board.XDIM = 20;
	Board.YDIM = 20;
	
	Board.prototype.render = function() {
		var newGrid = new Array(Board.YDIM);
		for( i = 0; i < Board.YDIM; i++ ) {
			newGrid[i] = [];
			for( j = 0; j< Board.XDIM; j++ ) {
				newGrid[i].push(".");
			};
		};

		_.each(this.snake.segments, function(segment) {
			newGrid[segment.y][segment.x] = "S";
		});
		return _(newGrid).map(function (row) { return row.join(""); }).join("\n");
	}
	
	
	var Coord = SnakeGame.Coord = function(y, x) {
		this.x = x;
		this.y = y;
	}
	
	
	
})(this);